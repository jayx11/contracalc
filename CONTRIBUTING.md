## Getting Started

To start contributing, follow these steps:

1. **Fork the Repository**: Click the "Fork" button at the top-right corner of the repository's page to create your own copy of the project.

2. **Clone Your Fork**: Clone your fork of the repository to your local machine using Git. 

    ```
    git clone https://gitlab.com/jayx11/contracalc.git
    ```

3. **Create a Branch**: Create a new branch for your changes. Use a descriptive branch name that reflects the purpose of your changes.

    ```
    git checkout -b feature/new-feature
    ```

4. **Make Changes**: Make your desired changes to the codebase.

5. **Commit Changes**: Commit your changes with a descriptive commit message.

    ```
    git commit -m "Add new feature"
    ```

6. **Push Changes**: Push your changes to your fork on GitLab.

    ```
    git push origin feature/new-feature
    ```

7. **Open a Pull Request**: Go to the [project's repository](https://gitlab.com/jayx11/contracalc/) on GitLab and open a pull request from your forked branch to the `main` branch of the original repository.

## Code Style

Please follow the existing code style and conventions used in the project. If there are no specific guidelines, try to match the existing code as closely as possible.

## Testing

If your changes include new features or modifications to existing features, please include relevant tests to ensure that everything works as expected.

## Reporting Issues

If you encounter any bugs, issues, or have feature requests, please [open an issue](https://gitlab.com/jayx11/contracalc/issues) on GitLab. Provide as much detail as possible, including steps to reproduce the issue and any relevant information about your environment.

## License

By contributing to this project, you agree that your contributions will be licensed under the [project's license](LICENSE.md). All contributions are subject to the terms and conditions of the license.

## Get Help

If you need help or have questions about contributing to the project, feel free to reach out at [this email](mailto:diego@veracordova.xyz)
