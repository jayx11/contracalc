# contracalc
## an open source calculator tailored towards woodworking and construction.

## Installation
work in progress, check back later

## Usage
wip
## Support
please use gitlab's issue tracker
## Roadmap
- [ ] get the calc functional
- [ ] make it pretty
- [ ] publish
- [ ] get the pwa functional
- [ ] publish
- [ ] work on native ios if possible

## Contributing
see [CONTRIBUTING](CONTRIBUTING.md)
## License
see [LICENSE](LICENSE.md)

